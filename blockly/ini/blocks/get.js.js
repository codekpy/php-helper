Blockly.PHP['get_get'] = function(block) {
  var value_get_key = Blockly.PHP.valueToCode(block, 'get_key', Blockly.PHP.ORDER_ATOMIC);
  // TODO: Assemble PHP into code variable.
  var l = value_get_key;
  var value_get_key = l.slice(2, l.length - 1);
  var code = '$_GET["'  + value_get_key + '"]';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.PHP.ORDER_NONE];
};

Blockly.PHP['get_post'] = function(block) {
  var value_post_key = Blockly.PHP.valueToCode(block, 'post_key', Blockly.PHP.ORDER_ATOMIC);
  // TODO: Assemble PHP into code variable.
  var l = value_get_key;
  var value_get_key = l.slice(2, l.length - 1);
  var code = '$_POST["'  + value_post_key + '"]';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.PHP.ORDER_NONE];
};

Blockly.PHP['get_cookie'] = function(block) {
  var value_cookie_key = Blockly.PHP.valueToCode(block, 'cookie_key', Blockly.PHP.ORDER_ATOMIC);
  // TODO: Assemble PHP into code variable.
  var l = value_cookie_key;
  var value_cookie_key = l.slice(2, l.length - 1);
  var code = '$_COOKIE["'  + value_cookie_key + '"]';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.PHP.ORDER_NONE];
};

Blockly.PHP['php_start'] = function(block) {
  var statements_php_start = Blockly.PHP.statementToCode(block, 'php-start');
  // TODO: Assemble PHP into code variable.
  var code = '&lt;?php\n' + statements_php_start + '\n?&gt;';
  return code;
};

Blockly.PHP['read'] = function(block) {
  var value_name = Blockly.PHP.valueToCode(block, 'name', Blockly.PHP.ORDER_ATOMIC);
  var l = value_name;
  var value_name = l.slice(2, l.length - 1);
  var text_bianname = block.getFieldValue('bianname');
  var value_return_text = Blockly.PHP.valueToCode(block, 'return_text', Blockly.PHP.ORDER_ATOMIC);
  // TODO: Assemble PHP into code variable.
  var code = `$` + text_bianname + ` = fopen("` + value_name + `", "r") or die ("` + value_return_text + `");<br>` 
             + '$' + text_bianname + ' = fread(' + '$' + text_bianname + ',filesize("' + value_name + '"));<br>' 
             + 'fclose($' + text_bianname + ');<br>'
  return code;
};

Blockly.PHP['write'] = function(block) {
  var value_path = Blockly.PHP.valueToCode(block, 'path', Blockly.PHP.ORDER_ATOMIC);
  var l = value_path;
  var value_path = l.slice(2, l.length - 1);
  var text_name = block.getFieldValue('name');//变量名
  var value_write_text = Blockly.PHP.valueToCode(block, 'write_text', Blockly.PHP.ORDER_ATOMIC);
  // TODO: Assemble PHP into code variable.
  var code = '$' + text_name + ' = fopen("' + value_path + '", "w") or die ("' + 'Unable to open file!' + '");<br>' + '$' + text_name + ' = fwrite(' + '$' + text_name + ',"' + value_path + '");<br>' + 'fclose($' + text_name + ');<br>';
  return code;
};

Blockly.PHP['print_r'] = function(block) {
  var value_name = Blockly.PHP.valueToCode(block, 'NAME', Blockly.PHP.ORDER_ATOMIC);
  // TODO: Assemble PHP into code variable.
  var code = 'print_r(' + value_name + ');<br>';
  return code;
};

Blockly.PHP['echo'] = function(block) {
  var value_name = Blockly.PHP.valueToCode(block, 'NAME', Blockly.PHP.ORDER_ATOMIC);
  // TODO: Assemble PHP into code variable.
  var code = 'echo ' + value_name + ';<br>';
  return code;
};

Blockly.PHP['htmlspecialchars'] = function(block) {
  var value_name = Blockly.PHP.valueToCode(block, 'NAME', Blockly.PHP.ORDER_ATOMIC);
  // TODO: Assemble PHP into code variable.
  var code = 'htmlentities(' + value_name + ')';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.PHP.ORDER_NONE];
};

Blockly.PHP['mysql_close'] = function(block) {
  var value_example = Blockly.PHP.valueToCode(block, 'example', Blockly.PHP.ORDER_ATOMIC);
  var value_name = Blockly.PHP.valueToCode(block, 'name', Blockly.PHP.ORDER_ATOMIC);
  // TODO: Assemble PHP into code variable.
  var code = 'mysql_close($' + value_example + ');<br>';
  return code;
};

Blockly.PHP['css'] = function(block) {
  var statements_name = Blockly.PHP.statementToCode(block, 'NAME');
  // TODO: Assemble PHP into code variable.
  var code = 'style="' + statements_name + '" ';
  return code;
};

Blockly.PHP['mysql_connect'] = function(block) {
  var value_example = Blockly.PHP.valueToCode(block, 'example', Blockly.PHP.ORDER_ATOMIC);
  var l = value_example;
  var value_example = l.slice(1, l.length - 1);
  var value_url = Blockly.PHP.valueToCode(block, 'url', Blockly.PHP.ORDER_ATOMIC);
  var l = value_url;
  var value_url = l.slice(1, l.length - 1);
  var value_user_name = Blockly.PHP.valueToCode(block, 'user_name', Blockly.PHP.ORDER_ATOMIC);
  var l = value_user_name;
  var value_user_name = l.slice(1, l.length - 1);
  var value_key = Blockly.PHP.valueToCode(block, 'key', Blockly.PHP.ORDER_ATOMIC);
  var l = value_key;
  var value_key = l.slice(1, l.length - 1);
  // TODO: Assemble PHP into code variable.
  var code = '$' + value_example + ' = mysql_connect("' + value_url + '","' + value_user_name + '","' + value_key + '");<br>' + 'if (!$' + value_example + ')<br>{<br>die("Could not connect: ".mysql_error());<br>}<br>';
  return code;
};

Blockly.PHP['mail'] = function(block) {
  var value_user = Blockly.PHP.valueToCode(block, 'user', Blockly.PHP.ORDER_ATOMIC);
  var value_main = Blockly.PHP.valueToCode(block, 'main', Blockly.PHP.ORDER_ATOMIC);
  var value_text = Blockly.PHP.valueToCode(block, 'text', Blockly.PHP.ORDER_ATOMIC);
  var value_h = Blockly.PHP.valueToCode(block, 'h', Blockly.PHP.ORDER_ATOMIC);
  var value_name = Blockly.PHP.valueToCode(block, 'NAME', Blockly.PHP.ORDER_ATOMIC);
  // TODO: Assemble PHP into code variable.
  var code = 'mail('+value_user+','+value_main+','+value_text+','+value_h+','+value_h+','+value_name+');<br>';
  return code;
};
Blockly.PHP['html'] = function(block) {
  var statements_name = Blockly.PHP.statementToCode(block, 'NAME');
  // TODO: Assemble PHP into code variable.
  var code = '&lt;html&gt;<br>' + statements_name + '&lt;/html&gt;<br>';
  return code;
};
Blockly.PHP['head'] = function(block) {
  var statements_name = Blockly.PHP.statementToCode(block, 'NAME');
  // TODO: Assemble PHP into code variable.
  var code = '&lt;head&gt;<br>' + statements_name + '<br>&lt;/head&gt;<br>';
  return code;
};

Blockly.PHP['diycode'] = function(block) {
  var text_code = block.getFieldValue('code');
  // TODO: Assemble PHP into code variable.
  var code = text_code;
  return code;
};
Blockly.PHP['h_'] = function(block) {
  var dropdown_test = block.getFieldValue('test');
  var statements_name = Blockly.PHP.statementToCode(block, 'NAME');
  var statements_attribute = Blockly.PHP.statementToCode(block, 'attribute');
  // TODO: Assemble PHP into code variable.
  var code = '&lt;'+dropdown_test + ' ' + statements_attribute + '&gt;<br>'+statements_name+'<br>&lt;/'+dropdown_test+'&gt;<br>';
  return code;
};

Blockly.PHP['body'] = function(block) {
  var statements_name = Blockly.PHP.statementToCode(block, 'NAME');
  var statements_attribute = Blockly.PHP.statementToCode(block, 'attribute');
  // TODO: Assemble PHP into code variable.
  var code = '&lt;body ' + statements_attribute +'&gt;<br>' + statements_name + '<br>&lt;/body&gt;<br>';
  return code;
};

Blockly.PHP['html5'] = function(block) {
  // TODO: Assemble PHP into code variable.
  var code = '&lt;!DOCTYPE HTML&gt;<br>';
  return code;
};

Blockly.PHP['p'] = function(block) {
  var statements_name = Blockly.PHP.statementToCode(block, 'NAME');
  var statements_attribute = Blockly.PHP.statementToCode(block, 'attribute');
  // TODO: Assemble PHP into code variable.
  var code = '&lt;p ' + statements_attribute +'"&gt;<br>' + statements_name + '<br>&lt;/p&gt;<br>';
  return code;
};

Blockly.PHP['img'] = function(block) {
  var text_src = block.getFieldValue('src');
  var number_width = block.getFieldValue('width');
  var number_height = block.getFieldValue('height');
  // TODO: Assemble PHP into code variable.
  var code = '&lt;img '+' src="'+text_src+'"'+' width="'+number_width+'"'+' heigth="'+number_height+'"'+'"&gt;<br>' + statements_name + '<br>';
  return code;
};

Blockly.PHP['title'] = function(block) {
  var value_name = Blockly.PHP.valueToCode(block, 'NAME', Blockly.PHP.ORDER_ATOMIC);
  // TODO: Assemble PHP into code variable.
  var l = value_name;
  var value_name = l.slice(1, l.length - 1);
  var code = '&lt;title&gt;' + value_name + '&lt;/title&gt;<br>';
  return code;
};

Blockly.PHP['diycode2'] = function(block) {
  var text_code = block.getFieldValue('code');

  return [text_code, Blockly.PHP.ORDER_NONE];
};

Blockly.PHP['br'] = function(block) {
  // TODO: Assemble PHP into code variable.
  var code = '&lt;br&gt;';
  return code;
};
Blockly.PHP['a'] = function(block) {
  var text_link = block.getFieldValue('link');
  var statements_name = Blockly.PHP.statementToCode(block, 'NAME');
  var statements_attribute = Blockly.PHP.statementToCode(block, 'attribute');
  // TODO: Assemble PHP into code variable.
  var code = '&lt;img '+' src="'+text_link+'" '+statements_attribute+'&gt;'+ statements_name + '&lt;/a&gt;<br>';
  return code;
};

Blockly.PHP['video'] = function(block) {
  var text_src = block.getFieldValue('src');
  var number_width = block.getFieldValue('width');
  var number_height = block.getFieldValue('height');
  // TODO: Assemble PHP into code variable.
  var code = '&lt;video '+' src="'+text_src+'"'+' width="'+number_width+'"'+' heigth="'+number_height+'"'+'"&gt;<br>&lt;/video&gt;<br>';
  return code;
};
Blockly.PHP['shuxing'] = function(block) {
  var text_name = block.getFieldValue('name');
  var value_value = Blockly.PHP.valueToCode(block, 'value', Blockly.PHP.ORDER_ATOMIC);
  // TODO: Assemble PHP into code variable.
  var l = value_value;
  var value_value = l.slice(1, l.length - 1);
  var code = text_name + ':' + value_value + ' ';
  return code;
};

Blockly.PHP['width'] = function(block) {
  var value_width_value = Blockly.PHP.valueToCode(block, 'width_value', Blockly.PHP.ORDER_ATOMIC);
  // TODO: Assemble PHP into code variable.
  console.log(value_width_value);
  var code = 'width:' + value_width_value + ';';
  return code;
};

Blockly.PHP['height'] = function(block) {
  var value_height_value = Blockly.PHP.valueToCode(block, 'height_value', Blockly.PHP.ORDER_ATOMIC);
  // TODO: Assemble PHP into code variable.
  var code = 'height:' + value_height_value + ';';
  return code;
};

Blockly.PHP['point'] = function(block) {
  var value_value = Blockly.PHP.valueToCode(block, 'value', Blockly.PHP.ORDER_ATOMIC);
  // TODO: Assemble PHP into code variable.

  var code = value_value + '%';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.PHP.ORDER_NONE];

};

Blockly.PHP['px'] = function(block) {
  var value_value = Blockly.PHP.valueToCode(block, 'value', Blockly.PHP.ORDER_ATOMIC);
  // TODO: Assemble PHP into code variable.

  var code = value_value + 'px';
  console.log(code);
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.PHP.ORDER_NONE];
};

Blockly.PHP['style_first'] = function(block) {
  var dropdown_style_name = block.getFieldValue('style_name');
  var dropdown_unit = block.getFieldValue('unit');
  var value_name = Blockly.PHP.valueToCode(block, 'NAME', Blockly.PHP.ORDER_ATOMIC);
  // TODO: Assemble PHP into code variable.
  var code = dropdown_style_name + ':' + value_name + dropdown_unit;
  return code;
};