Blockly.Blocks['get_get'] = {
  init: function() {
    this.appendValueInput("get_key")
        .setCheck("String")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("获取get信息 key");
    this.setInputsInline(true);
    this.setOutput(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['get_post'] = {
  init: function() {
    this.appendValueInput("post_key")
        .setCheck("String")
        .appendField("获取post信息 key");
    this.setInputsInline(true);
    this.setOutput(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['get_cookie'] = {
  init: function() {
    this.appendValueInput("cookie_key")
        .setCheck("String")
        .appendField("获取cookie值 Key:");
    this.setInputsInline(true);
    this.setOutput(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['php_start'] = {
  init: function() {
    this.appendStatementInput("php-start")
        .setCheck(null)
        .appendField("PHP积木块");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("<?php 代码 ?>");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['read'] = {
  init: function() {
    this.appendValueInput("name")
        .setCheck(null)
        .appendField("读取文件 文件名(路径):");
    this.appendDummyInput()
        .appendField("存放进变量")
        .appendField(new Blockly.FieldTextInput("myfile"), "bianname");
    this.appendValueInput("return_text")
        .setCheck(null)
        .appendField("  读取失败后的返回文本");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(135);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['write'] = {
  init: function() {
    this.appendValueInput("path")
        .setCheck(null)
        .appendField("写入文件 文件名：");
    this.appendDummyInput()
        .appendField("用于操作的变量名")
        .appendField(new Blockly.FieldTextInput("default"), "name");
    this.appendValueInput("write_text")
        .setCheck(null)
        .appendField(" 要写入的内容");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['echo'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck(null)
        .appendField("echo输出");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(165);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['print_r'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck(null)
        .appendField("print_r输出");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(180);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['htmlspecialchars'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck("String")
        .appendField("把文本");
    this.appendDummyInput()
        .appendField("转换为 HTML 实体");
    this.setOutput(true, null);
    this.setColour(0);
 this.setTooltip("转义");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['mysql_connect'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("连接MySQL 数据库");
    this.appendValueInput("example")
        .setCheck(null)
        .appendField("对象名");
    this.appendValueInput("url")
        .setCheck("String")
        .appendField("连接的服务器：");
    this.appendValueInput("user_name")
        .setCheck("String")
        .appendField("用户名：");
    this.appendValueInput("key")
        .setCheck("String")
        .appendField("密码：");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("连接MySQL 数据库");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['mail'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("发送邮件");
    this.appendValueInput("user")
        .setCheck("String")
        .appendField("接收者");
    this.appendValueInput("main")
        .setCheck("String")
        .appendField("主题");
    this.appendValueInput("text")
        .setCheck("String")
        .appendField("发送的消息");
    this.appendValueInput("h")
        .setCheck("String")
        .appendField("附加的标题");
    this.appendValueInput("NAME")
        .setCheck("String")
        .appendField("邮件发送程序规定额外的参数");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("发送的消息应使用\\n来分隔各行，附加的标题应当使用\\r\\n分隔附加的标题");
 this.setHelpUrl("https://www.w3school.com.cn/php/php_ref_mail.asp");
  }
};
Blockly.Blocks['width'] = {
  init: function() {
    this.appendValueInput("width_value")
        .setCheck(null)
        .appendField("设置元素宽度为");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['height'] = {
  init: function() {
    this.appendValueInput("height_value")
        .setCheck(null)
        .appendField("设置元素高度为");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['point'] = {
  init: function() {
    this.appendValueInput("value")
        .setCheck("Number")
        .appendField("父元素的百分比");
    this.setInputsInline(true);
    this.setOutput(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['px'] = {
  init: function() {
    this.appendValueInput("value")
        .setCheck(null)
        .appendField("像素px ");
    this.setInputsInline(true);
    this.setOutput(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['style_first'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck("Number")
        .appendField(new Blockly.FieldDropdown([["宽度","width"], ["高度","height"]]), "style_name")
        .appendField(new Blockly.FieldDropdown([["百分比","%"], ["像素","px"]]), "unit");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['css'] = {
  init: function() {
    this.appendStatementInput("NAME")
        .setCheck(null)
        .appendField("style属性");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['html'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("一个网页");
    this.appendStatementInput("NAME")
        .setCheck(null);
    this.setPreviousStatement(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['diycode'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("在此输入任何代码（php、js、css......）")
        .appendField(new Blockly.FieldTextInput("……"), "code");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(75);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['mysql_close'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("结束数据库连接");
    this.appendValueInput("example")
        .setCheck(null)
        .appendField("对象名");
    this.appendValueInput("name")
        .setCheck("String")
        .appendField("数据库名");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['head'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("头部");
    this.appendStatementInput("NAME")
        .setCheck(null);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['body'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("身体");
    this.appendStatementInput("NAME")
        .setCheck(null);
    this.appendDummyInput()
        .appendField("属性");
    this.appendStatementInput("attribute")
        .setCheck(null);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['title'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck("String")
        .appendField("标题：");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(160);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['html5'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("HTML5标准网页声明");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['h_'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldDropdown([["一级","h1"], ["二级","h2"], ["三级","h3"], ["四级","h4"], ["五级","h5"], ["六级","h6"]]), "test")
        .appendField("文本");
    this.appendDummyInput()
        .appendField("内容");
    this.appendStatementInput("NAME")
        .setCheck(null);
    this.appendDummyInput()
        .appendField("属性");
    this.appendStatementInput("attribute")
        .setCheck(null);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(65);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['p'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("段落标签");
    this.appendDummyInput()
        .appendField("内容");
    this.appendStatementInput("NAME")
        .setCheck(null);
    this.appendDummyInput()
        .appendField("属性");
    this.appendStatementInput("attribute")
        .setCheck(null);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(65);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['img'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("图像")
        .appendField(new Blockly.FieldTextInput("来源"), "src")
        .appendField("宽")
        .appendField(new Blockly.FieldNumber(100), "width")
        .appendField("高")
        .appendField(new Blockly.FieldNumber(100), "height");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(315);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['a'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("超链接")
        .appendField(new Blockly.FieldTextInput("https://www.w3school.com.cn/html"), "link");
    this.appendDummyInput()
        .appendField("内容");
    this.appendStatementInput("NAME")
        .setCheck(null);
    this.appendDummyInput()
        .appendField("属性");
    this.appendStatementInput("attribute")
        .setCheck(null);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(195);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['video'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("视频")
        .appendField(new Blockly.FieldTextInput("来源"), "src")
        .appendField("宽")
        .appendField(new Blockly.FieldNumber(100), "width")
        .appendField("高")
        .appendField(new Blockly.FieldNumber(100), "height");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(315);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['br'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("换行");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(180);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};
Blockly.Blocks['shuxing'] = {
  init: function() {
    this.appendValueInput("value")
        .setCheck("String")
        .appendField("属性名")
        .appendField(new Blockly.FieldTextInput("name"), "name")
        .appendField("属性值");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['diycode2'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("在此输入任何代码")
        .appendField(new Blockly.FieldTextInput("……"), "code");
    this.setOutput(true, null);
    this.setColour(75);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['css'] = {
  init: function() {
    this.appendStatementInput("NAME")
        .setCheck(null)
        .appendField("CSS样式（属性）");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};
