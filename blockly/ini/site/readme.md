# 免责声明


本开源程序根目录下网页`index.php`的源代码由[CoCo中控台](https://gitee.com/coco-central)制作，
[PHP助手贡献人员](https://gitee.com/codekpy/php-helper/contributors?ref=master)修改，
运营于[PHP助手官网](http://www.codekpy.site/php-helper-master/index.php)，
借鉴[微信](https://weixin.qq.com/)的设计风格。

在不违反法律的前提下，该源代码**允许任何形式的使用**
（包括但不限于学习、公益、商用等用途），
在使用中，**不强制要求注明制作方**。
产生的法律问题，CoCo中控台**不负连带责任**。

![图片](./ico.jpg)
特此声明