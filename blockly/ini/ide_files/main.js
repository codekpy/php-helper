Blockly.Blocks['get_get'] = {
  init: function() {
    this.appendValueInput("get_key")
        .setCheck("String")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("获取get信息 key");
    this.setInputsInline(true);
    this.setOutput(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['get_post'] = {
  init: function() {
    this.appendValueInput("post_key")
        .setCheck("String")
        .appendField("获取post信息 key");
    this.setInputsInline(true);
    this.setOutput(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['get_cookie'] = {
  init: function() {
    this.appendValueInput("cookie_key")
        .setCheck("String")
        .appendField("获取cookie值 Key:");
    this.setInputsInline(true);
    this.setOutput(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['php_start'] = {
  init: function() {
    this.appendStatementInput("php-start")
        .setCheck(null)
        .appendField("PHP积木块");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("<?php 代码 ?>");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['read'] = {
  init: function() {
    this.appendValueInput("name")
        .setCheck(null)
        .appendField("读取文件 文件名(路径):");
    this.appendDummyInput()
        .appendField("存放进变量")
        .appendField(new Blockly.FieldTextInput("myfile"), "bianname");
    this.appendValueInput("return_text")
        .setCheck(null)
        .appendField("  读取失败后的返回文本");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(135);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['write'] = {
  init: function() {
    this.appendValueInput("path")
        .setCheck(null)
        .appendField("写入文件 文件名：");
    this.appendDummyInput()
        .appendField("用于操作的变量名")
        .appendField(new Blockly.FieldTextInput("default"), "name");
    this.appendValueInput("write_text")
        .setCheck(null)
        .appendField(" 要写入的内容");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['echo'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck(null)
        .appendField("echo输出");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(165);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['print_r'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck(null)
        .appendField("print_r输出");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(180);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['htmlspecialchars'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck("String")
        .appendField("把文本");
    this.appendDummyInput()
        .appendField("转换为 HTML 实体");
    this.setOutput(true, null);
    this.setColour(0);
 this.setTooltip("转义");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['mysql_connect'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("连接MySQL 数据库");
    this.appendValueInput("example")
        .setCheck(null)
        .appendField("对象名");
    this.appendValueInput("url")
        .setCheck("String")
        .appendField("连接的服务器：");
    this.appendValueInput("user_name")
        .setCheck("String")
        .appendField("用户名：");
    this.appendValueInput("key")
        .setCheck("String")
        .appendField("密码：");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("连接MySQL 数据库");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['mail'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("发送邮件");
    this.appendValueInput("user")
        .setCheck("String")
        .appendField("接收者");
    this.appendValueInput("main")
        .setCheck("String")
        .appendField("主题");
    this.appendValueInput("text")
        .setCheck("String")
        .appendField("发送的消息");
    this.appendValueInput("h")
        .setCheck("String")
        .appendField("附加的标题");
    this.appendValueInput("NAME")
        .setCheck("String")
        .appendField("邮件发送程序规定额外的参数");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("发送的消息应使用\\n来分隔各行，附加的标题应当使用\\r\\n分隔附加的标题");
 this.setHelpUrl("https://www.w3school.com.cn/php/php_ref_mail.asp");
  }
};
Blockly.Blocks['width'] = {
  init: function() {
    this.appendValueInput("width_value")
        .setCheck(null)
        .appendField("设置元素宽度为");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['height'] = {
  init: function() {
    this.appendValueInput("height_value")
        .setCheck(null)
        .appendField("设置元素高度为");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['point'] = {
  init: function() {
    this.appendValueInput("value")
        .setCheck("Number")
        .appendField("父元素的百分比");
    this.setInputsInline(true);
    this.setOutput(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['px'] = {
  init: function() {
    this.appendValueInput("value")
        .setCheck(null)
        .appendField("像素px ");
    this.setInputsInline(true);
    this.setOutput(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['style_first'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck("Number")
        .appendField(new Blockly.FieldDropdown([["宽度","width"], ["高度","height"]]), "style_name")
        .appendField(new Blockly.FieldDropdown([["百分比","point"], ["像素","px"]]), "unit");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['css'] = {
  init: function() {
    this.appendStatementInput("NAME")
        .setCheck(null)
        .appendField("style属性");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['html'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("一个网页");
    this.appendStatementInput("NAME")
        .setCheck(null);
    this.setPreviousStatement(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['diycode'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("在此输入任何代码（php、js、css......）")
        .appendField(new Blockly.FieldTextInput("……"), "code");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(75);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['mysql_close'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("结束数据库连接");
    this.appendValueInput("example")
        .setCheck(null)
        .appendField("对象名");
    this.appendValueInput("name")
        .setCheck("String")
        .appendField("数据库名");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['head'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("头部");
    this.appendStatementInput("NAME")
        .setCheck(null);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['body'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("身体");
    this.appendStatementInput("NAME")
        .setCheck(null);
    this.appendDummyInput()
        .appendField("属性");
    this.appendStatementInput("attribute")
        .setCheck(null);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['title'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck("String")
        .appendField("标题：");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(160);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['html5'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("HTML5标准网页声明");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['h_'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldDropdown([["一级","h1"], ["二级","h2"], ["三级","h3"], ["四级","h4"], ["五级","h5"], ["六级","h6"]]), "test")
        .appendField("文本");
    this.appendDummyInput()
        .appendField("内容");
    this.appendStatementInput("NAME")
        .setCheck(null);
    this.appendDummyInput()
        .appendField("属性");
    this.appendStatementInput("attribute")
        .setCheck(null);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(65);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['p'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("段落标签");
    this.appendDummyInput()
        .appendField("内容");
    this.appendStatementInput("NAME")
        .setCheck(null);
    this.appendDummyInput()
        .appendField("属性");
    this.appendStatementInput("attribute")
        .setCheck(null);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(65);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['img'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("图像")
        .appendField(new Blockly.FieldTextInput("来源"), "src")
        .appendField("宽")
        .appendField(new Blockly.FieldNumber(100), "width")
        .appendField("高")
        .appendField(new Blockly.FieldNumber(100), "height");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(315);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['a'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("超链接")
        .appendField(new Blockly.FieldTextInput("https://www.w3school.com.cn/html"), "link");
    this.appendDummyInput()
        .appendField("内容");
    this.appendStatementInput("NAME")
        .setCheck(null);
    this.appendDummyInput()
        .appendField("属性");
    this.appendStatementInput("attribute")
        .setCheck(null);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(195);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['video'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("视频")
        .appendField(new Blockly.FieldTextInput("来源"), "src")
        .appendField("宽")
        .appendField(new Blockly.FieldNumber(100), "width")
        .appendField("高")
        .appendField(new Blockly.FieldNumber(100), "height");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(315);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['br'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("换行");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(180);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};
Blockly.Blocks['shuxing'] = {
  init: function() {
    this.appendValueInput("value")
        .setCheck("String")
        .appendField("属性名")
        .appendField(new Blockly.FieldTextInput("name"), "name")
        .appendField("属性值");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['diycode2'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("在此输入任何代码")
        .appendField(new Blockly.FieldTextInput("……"), "code");
    this.setOutput(true, null);
    this.setColour(75);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['css'] = {
  init: function() {
    this.appendStatementInput("NAME")
        .setCheck(null)
        .appendField("CSS样式（属性）");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.PHP['get_get'] = function(block) {
  var value_get_key = Blockly.PHP.valueToCode(block, 'get_key', Blockly.PHP.ORDER_ATOMIC);
  // TODO: Assemble PHP into code variable.
  var l = value_get_key;
  var value_get_key = l.slice(2, l.length - 1);
  var code = '$_GET["'  + value_get_key + '"]';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.PHP.ORDER_NONE];
};

Blockly.PHP['get_post'] = function(block) {
  var value_post_key = Blockly.PHP.valueToCode(block, 'post_key', Blockly.PHP.ORDER_ATOMIC);
  // TODO: Assemble PHP into code variable.
  var l = value_get_key;
  var value_get_key = l.slice(2, l.length - 1);
  var code = '$_POST["'  + value_post_key + '"]';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.PHP.ORDER_NONE];
};

Blockly.PHP['get_cookie'] = function(block) {
  var value_cookie_key = Blockly.PHP.valueToCode(block, 'cookie_key', Blockly.PHP.ORDER_ATOMIC);
  // TODO: Assemble PHP into code variable.
  var l = value_cookie_key;
  var value_cookie_key = l.slice(2, l.length - 1);
  var code = '$_COOKIE["'  + value_cookie_key + '"]';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.PHP.ORDER_NONE];
};

Blockly.PHP['php_start'] = function(block) {
  var statements_php_start = Blockly.PHP.statementToCode(block, 'php-start');
  // TODO: Assemble PHP into code variable.
  var code = '&lt;?php\n' + statements_php_start + '\n?&gt;';
  return code;
};

Blockly.PHP['read'] = function(block) {
  var value_name = Blockly.PHP.valueToCode(block, 'name', Blockly.PHP.ORDER_ATOMIC);
  var l = value_name;
  var value_name = l.slice(2, l.length - 1);
  var text_bianname = block.getFieldValue('bianname');
  var value_return_text = Blockly.PHP.valueToCode(block, 'return_text', Blockly.PHP.ORDER_ATOMIC);
  // TODO: Assemble PHP into code variable.
  var code = `$` + text_bianname + ` = fopen("` + value_name + `", "r") or die ("` + value_return_text + `");<br>` 
             + '$' + text_bianname + ' = fread(' + '$' + text_bianname + ',filesize("' + value_name + '"));<br>' 
             + 'fclose($' + text_bianname + ');<br>'
  return code;
};

Blockly.PHP['write'] = function(block) {
  var value_path = Blockly.PHP.valueToCode(block, 'path', Blockly.PHP.ORDER_ATOMIC);
  var l = value_path;
  var value_path = l.slice(2, l.length - 1);
  var text_name = block.getFieldValue('name');//变量名
  var value_write_text = Blockly.PHP.valueToCode(block, 'write_text', Blockly.PHP.ORDER_ATOMIC);
  // TODO: Assemble PHP into code variable.
  var code = '$' + text_name + ' = fopen("' + value_path + '", "w") or die ("' + 'Unable to open file!' + '");<br>' + '$' + text_name + ' = fwrite(' + '$' + text_name + ',"' + value_path + '");<br>' + 'fclose($' + text_name + ');<br>';
  return code;
};

Blockly.PHP['print_r'] = function(block) {
  var value_name = Blockly.PHP.valueToCode(block, 'NAME', Blockly.PHP.ORDER_ATOMIC);
  // TODO: Assemble PHP into code variable.
  var code = 'print_r(' + value_name + ');<br>';
  return code;
};

Blockly.PHP['echo'] = function(block) {
  var value_name = Blockly.PHP.valueToCode(block, 'NAME', Blockly.PHP.ORDER_ATOMIC);
  // TODO: Assemble PHP into code variable.
  var code = 'echo ' + value_name + ';<br>';
  return code;
};

Blockly.PHP['htmlspecialchars'] = function(block) {
  var value_name = Blockly.PHP.valueToCode(block, 'NAME', Blockly.PHP.ORDER_ATOMIC);
  // TODO: Assemble PHP into code variable.
  var code = 'htmlentities(' + value_name + ')';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.PHP.ORDER_NONE];
};

Blockly.PHP['mysql_close'] = function(block) {
  var value_example = Blockly.PHP.valueToCode(block, 'example', Blockly.PHP.ORDER_ATOMIC);
  var value_name = Blockly.PHP.valueToCode(block, 'name', Blockly.PHP.ORDER_ATOMIC);
  // TODO: Assemble PHP into code variable.
  var code = 'mysql_close($' + value_example + ');<br>';
  return code;
};

Blockly.PHP['css'] = function(block) {
  var statements_name = Blockly.PHP.statementToCode(block, 'NAME');
  // TODO: Assemble PHP into code variable.
  var code = 'style="' + statements_name + '" ';
  return code;
};

Blockly.PHP['mysql_connect'] = function(block) {
  var value_example = Blockly.PHP.valueToCode(block, 'example', Blockly.PHP.ORDER_ATOMIC);
  var l = value_example;
  var value_example = l.slice(1, l.length - 1);
  var value_url = Blockly.PHP.valueToCode(block, 'url', Blockly.PHP.ORDER_ATOMIC);
  var l = value_url;
  var value_url = l.slice(1, l.length - 1);
  var value_user_name = Blockly.PHP.valueToCode(block, 'user_name', Blockly.PHP.ORDER_ATOMIC);
  var l = value_user_name;
  var value_user_name = l.slice(1, l.length - 1);
  var value_key = Blockly.PHP.valueToCode(block, 'key', Blockly.PHP.ORDER_ATOMIC);
  var l = value_key;
  var value_key = l.slice(1, l.length - 1);
  // TODO: Assemble PHP into code variable.
  var code = '$' + value_example + ' = mysql_connect("' + value_url + '","' + value_user_name + '","' + value_key + '");<br>' + 'if (!$' + value_example + ')<br>{<br>die("Could not connect: ".mysql_error());<br>}<br>';
  return code;
};

Blockly.PHP['mail'] = function(block) {
  var value_user = Blockly.PHP.valueToCode(block, 'user', Blockly.PHP.ORDER_ATOMIC);
  var value_main = Blockly.PHP.valueToCode(block, 'main', Blockly.PHP.ORDER_ATOMIC);
  var value_text = Blockly.PHP.valueToCode(block, 'text', Blockly.PHP.ORDER_ATOMIC);
  var value_h = Blockly.PHP.valueToCode(block, 'h', Blockly.PHP.ORDER_ATOMIC);
  var value_name = Blockly.PHP.valueToCode(block, 'NAME', Blockly.PHP.ORDER_ATOMIC);
  // TODO: Assemble PHP into code variable.
  var code = 'mail('+value_user+','+value_main+','+value_text+','+value_h+','+value_h+','+value_name+');<br>';
  return code;
};
Blockly.PHP['html'] = function(block) {
  var statements_name = Blockly.PHP.statementToCode(block, 'NAME');
  // TODO: Assemble PHP into code variable.
  var code = '&lt;html&gt;<br>' + statements_name + '&lt;/html&gt;<br>';
  return code;
};
Blockly.PHP['head'] = function(block) {
  var statements_name = Blockly.PHP.statementToCode(block, 'NAME');
  // TODO: Assemble PHP into code variable.
  var code = '&lt;head&gt;<br>' + statements_name + '<br>&lt;/head&gt;<br>';
  return code;
};

Blockly.PHP['diycode'] = function(block) {
  var text_code = block.getFieldValue('code');
  // TODO: Assemble PHP into code variable.
  var code = text_code;
  return code;
};
Blockly.PHP['h_'] = function(block) {
  var dropdown_test = block.getFieldValue('test');
  var statements_name = Blockly.PHP.statementToCode(block, 'NAME');
  var statements_attribute = Blockly.PHP.statementToCode(block, 'attribute');
  // TODO: Assemble PHP into code variable.
  var code = '&lt;'+dropdown_test + ' ' + statements_attribute + '&gt;<br>'+statements_name+'<br>&lt;/'+dropdown_test+'&gt;<br>';
  return code;
};

Blockly.PHP['body'] = function(block) {
  var statements_name = Blockly.PHP.statementToCode(block, 'NAME');
  var statements_attribute = Blockly.PHP.statementToCode(block, 'attribute');
  // TODO: Assemble PHP into code variable.
  var code = '&lt;body ' + statements_attribute +'&gt;<br>' + statements_name + '<br>&lt;/body&gt;<br>';
  return code;
};

Blockly.PHP['html5'] = function(block) {
  // TODO: Assemble PHP into code variable.
  var code = '&lt;!DOCTYPE HTML&gt;<br>';
  return code;
};

Blockly.PHP['p'] = function(block) {
  var statements_name = Blockly.PHP.statementToCode(block, 'NAME');
  var statements_attribute = Blockly.PHP.statementToCode(block, 'attribute');
  // TODO: Assemble PHP into code variable.
  var code = '&lt;p ' + statements_attribute +'"&gt;<br>' + statements_name + '<br>&lt;/p&gt;<br>';
  return code;
};

Blockly.PHP['img'] = function(block) {
  var text_src = block.getFieldValue('src');
  var number_width = block.getFieldValue('width');
  var number_height = block.getFieldValue('height');
  // TODO: Assemble PHP into code variable.
  var code = '&lt;img '+' src="'+text_src+'"'+' width="'+number_width+'"'+' heigth="'+number_height+'"'+'"&gt;<br>' + statements_name + '<br>';
  return code;
};

Blockly.PHP['title'] = function(block) {
  var value_name = Blockly.PHP.valueToCode(block, 'NAME', Blockly.PHP.ORDER_ATOMIC);
  // TODO: Assemble PHP into code variable.
  var l = value_name;
  var value_name = l.slice(2, l.length - 1);
  var code = '&lt;title&gt;' + value_name + '&lt;/title&gt;<br>';
  return code;
};

Blockly.PHP['diycode2'] = function(block) {
  var text_code = block.getFieldValue('code');

  return [text_code, Blockly.PHP.ORDER_NONE];
};

Blockly.PHP['br'] = function(block) {
  // TODO: Assemble PHP into code variable.
  var code = '&lt;br&gt;';
  return code;
};
Blockly.PHP['a'] = function(block) {
  var text_link = block.getFieldValue('link');
  var statements_name = Blockly.PHP.statementToCode(block, 'NAME');
  var statements_attribute = Blockly.PHP.statementToCode(block, 'attribute');
  // TODO: Assemble PHP into code variable.
  var code = '&lt;img '+' src="'+text_link+'" '+statements_attribute+'&gt;'+ statements_name + '&lt;/a&gt;<br>';
  return code;
};

Blockly.PHP['video'] = function(block) {
  var text_src = block.getFieldValue('src');
  var number_width = block.getFieldValue('width');
  var number_height = block.getFieldValue('height');
  // TODO: Assemble PHP into code variable.
  var code = '&lt;video '+' src="'+text_src+'"'+' width="'+number_width+'"'+' heigth="'+number_height+'"'+'"&gt;<br>&lt;/video&gt;<br>';
  return code;
};
Blockly.PHP['shuxing'] = function(block) {
  var text_name = block.getFieldValue('name');
  var value_value = Blockly.PHP.valueToCode(block, 'value', Blockly.PHP.ORDER_ATOMIC);
  // TODO: Assemble PHP into code variable.
  var l = value_value;
  var value_value = l.slice(1, l.length - 1);
  var code = text_name + ':' + value_value + ';<br>';
  return code;
};

Blockly.PHP['width'] = function(block) {
  var value_width_value = Blockly.PHP.valueToCode(block, 'width_value', Blockly.PHP.ORDER_ATOMIC);
  // TODO: Assemble PHP into code variable.
  console.log(value_width_value);
  var code = 'width:' + value_width_value + ';';
  return code;
};

Blockly.PHP['height'] = function(block) {
  var value_height_value = Blockly.PHP.valueToCode(block, 'height_value', Blockly.PHP.ORDER_ATOMIC);
  // TODO: Assemble PHP into code variable.
  var code = 'height:' + value_height_value + ';';
  return code;
};

Blockly.PHP['point'] = function(block) {
  var value_value = Blockly.PHP.valueToCode(block, 'value', Blockly.PHP.ORDER_ATOMIC);
  // TODO: Assemble PHP into code variable.

  var code = value_value + '%';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.PHP.ORDER_NONE];

};

Blockly.PHP['px'] = function(block) {
  var value_value = Blockly.PHP.valueToCode(block, 'value', Blockly.PHP.ORDER_ATOMIC);
  // TODO: Assemble PHP into code variable.

  var code = value_value + 'px';
  console.log(code);
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.PHP.ORDER_NONE];
};

Blockly.PHP['style_first'] = function(block) {
  var dropdown_style_name = block.getFieldValue('style_name');
  var dropdown_unit = block.getFieldValue('unit');
  var value_name = Blockly.PHP.valueToCode(block, 'NAME', Blockly.PHP.ORDER_ATOMIC);
  // TODO: Assemble PHP into code variable.
  var code = dropdown_style_name + ':' + value_name + dropdown_unit;
  return code;
};
  console.log(`
  iiiiiiiii;;;;iiiiiiiiiiiiiiiii;;;;;;;;;;;;;;;;;;;;iiiiiiiiiiiiiiiiiiiiii1111111111111t1ttttttttttfffffffffffffLLLLLLLLLLLGGGGGGG
  iiiiiiiii;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;iiiiiiiiiiiiiiiiiiiii11111111111111tttttttttttfffffffffffffLLLLLLLLLLLGGGGGG
  iiiii;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;iiiiiiiiiiiiiiiiiiiiii111111111111ttttttttttttffffffffffffffLLLLLLLLLLLGGGGG
  iiii;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;iiiiiiiiiiiiiiiiiiiiiiiii1111111111111ttttttttttttffffffffffffffLLLLLLLLLLGGGGG
  iii;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;iiiiiiiiiiiiiiiiiiiiiiiii1111111111111tttttttttttttffffffffffffffLLLLLLLLLGGGGGG
  ii;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;iiiiiiiiiiiiiiiiiiiiiiiiii11111111111ttttttttttttttffffffffffffffffLLLLLLLGGGGGGG
  i;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;iiiiiiiiiiiiiiiiiiiiiiiii11111111111111tttttttttttttffffffffffffffffLLLLLLLLGGGGGGG
  ;;iii;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;iiiiiiiiiiiiiiiiiiiiiii1111111111111111t1ttttttttttfffffffffffffffffLLLLLLLLLLGGGGGGG
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;iiii;;;;;iiiiiiiiiiiiiiiiiiiiii1111111111111111tttttttttttttfffffffffffffffffLfLLLLLLLLLGGGGGGGG
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;iiiiiiiiiiiiiiiiiiiiiiiiii111111111111111111ttttttttttttfffffffffffffffffLLLLLLLLLLLGGGGGGGG
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;iiiiiiiiiiiiiiiiiiiiii11111111111111111111111ttttttttttttfffffffffffffffLLLLLLLLLLLGLGGGGGGG
  i;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;iiiiiiiiiiiiiiiiiiii11111111111111111111111111111tttttttttffffffffffffffLLLLLLLLLLLLLLLGGGGGGG
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;iiiiiiiiiiiiiiii1ii1111111111111111111111111111111111tttttttttttfffffffffffffLLLLLLLLLLLLLLGGGGGGGGG
  ;;;;;;;;;;;;;;;;;;;;iiiiiiiiiiiiiiiiiiiii111t                                       ffffffffffffffffffLLLLLLLLLLLLLLLGGGGGGGGGGG
  iiiii;;;;;;;;;;;;iiiiiiiiiiiiiiiiiiiiii1111tt                                       ffffffffffffffffLLLLLLLLLLLLLLLLLLGGGGGGGGGG
  iiiiii;i;;i;;;iiiiiiiiiiiiiiiiiiiiiii111111tf    0000000000888888888880000000000    LLfffffffffffffLLLLLLLLLLLLLLLLLGGGGGGGGGGGG
  iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii111111tttfL    8888888888888888888888888888880    LLLffffffffLLLLLLLLLLLLLLLLLLLGGGGGGGGGGGGGG
  iiiiiiiiiiiiiiiiiiiiiiiiiiii11111111111tttfLG    GGGGGGGGGGGCCCCCCCCCGGGGGGGGGC8    LLLLLLLLLLLLLLLLLLLLLLLLLLLLGGGGGGGGGGGGGGGG
  iiiiiiiiiiiiiiiiiiiii11111111111111111ttttfLG    GLLLLLLLLLLLLLG LLLGLLLLLLGGGGC    LLLLLLLLLLLLLLLLLLLLLLLLLLLGGGGGGGGGGGGGGGGG
  iiiiiiiiiiiiiiiiiiii1i111111111111111ttttffLG    GLLLLffffffL       LLLLLLLLGGGC    GLLLLLLLLLLLLLLLLLLLLLLLLLLGGGGGGGGGGGGGGGGG
  11111111111111ii1111111111111111111ttttttfLLG    GLLLLffft             LLLLLGGCC    GGLLLLLLLLLLLLLLLLLLLLLLLGGGGGGGGGGGGGGGGGGG
  11111111111111111111111111111111ttttttttffLLG    GLLLf.                   LLGGGC    GGLLLLLLLLLLLLLLLLLLLLGGGGGGGGGGGGGGGGGGGGGG
  111111111111111111111111111ttttttttttttfffLLG    GGLLLG0               00GGGGGCC    GGLLLLLLLLLLLLLLLLLGGGGGGGGGGGGGGGGGGGGGCGGC
  tttt111111111111111111ttttttttttttttttffffLLG    GGGGGGCC88         00CCCGGGGGCC    GGGLLLLLLLLLLLGGGGGGGGGGGGGGGGGGGGGGGGGCCCCC
  ttttttttttttttttttttttttttttttttttttffffffLGG    GGGGCC8CCC880  800CCCCCCCGGGGGC    CGGLLLLLLLLLGGGGGGGGGGGGGGGGGGGGGGGGGGCCCCCC
  ttttttttttttttttttttttttttttttttttfffffffLLGC      LGGCC88888000888C8CCCCCGGGL      CGGGGLLLLLLGGGGGGGGGGGGGGGGGGGGGGGGGGCCCCCCC
  ttttttttttttttttttttttttttttffffffffffffLLLGC0        GGCC8888888888CCCCGGG        fCGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGCCCCCCCCCC
  ffffffffffffffffftttfffffffffffffffffffLLLGGC8000        GGCCC888CCCCGGG        :008CCGGGGGGGGGGGGGGGGGGGGGGGGGGGGCCCCCCCCCCCCCC
  fffffffffffffffffffffffffffffffffffffffLLLGGC800000@        CGCCCCCGG        G000088CGGGGGGGGGGGGGGGGGGGGGGGGGGGGCCCCCCCCCCCCCCC
  ffffffffffffffffffffffffffffffffffffLLLLLLGGCC80000000G        GGL        800000008CCGGGGGGGGGGGGGGGGGGGGGGGGGGCCCCCCCCCCCCCCCCC
  fffffffffffffffffffffffffffffffLLLLLLLLLLLLGGCC8800000000L             00000000088CCCGGGGGGGGGGGGGGGGGGGGGGCCCCCCCCCCCCCCCCCCCCC
  LLLLLffffffffLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLGGGCCC8880000000@1       800000000888CCCCGGGGGGGGGGGGGGGGGGGCGCCCCCCCCCCCCCCCCCCCCCCC
  LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLGGGGGCCCC88800000000f C000000000888CCCCGGGGGGGGGGGGGGGGGCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
  LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLGGGGGGGGGCCCC888000000000000008888CCCCCCGGGGGGGGGGGGGGGGCGCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
  LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLGGGGGGGGGGGGGGGGGGGGGCCCC8880000000088888CCCCCCCGGGGGGGGGGGGGCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
  LLLLLLLLLLLLLLLLLLGGLLLLLLGGGGGGGGGGGGGGGGGGGGGGGGGCGCCCCC888888088888CCCCCCCCCCGCCCGCCCCGCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
  LLGLLLGGGGGGGGGGGGi      GGGGGGGGG       GGC             1CCCCC8C8       CCCCCCC       CCCCCCC              CCCCCCCCCCCCCCCCCCCC
  GGGGGGGGGGGGGGGGGG8      GGGGGGGCC       GGC                 CCCCCt      CCCCCCC       CCCCCCC                 LCCCCCCCCCCCCCCCC
  GGGGGGGGGGGGGGGGGC8      CGGGGGCC8       CC8       00000      :CC81      CCCCCC8       CCCCCC8       00000       CCCCCCCCCCCCCC8
  GGGGGGGGGGGGGGGGCC8      CGGGCCC88       C88       0000000     CC81      CCCCC88       CCCCCC8       0@@@000     88CCCCCCCCC8888
  GGGGGGGGGGGGGGGCCC8      CCCCCCC88       880       0888800     C88i      CCCC880       CCCCCC8       088880@     888CCCCCC888888
  GGGGGGGGGGGGGGCCC80                      880       00@00       880;                    CCCCC88       00000       88888CCC8888888
  GGGGGGGGGGGGGGCCC88                      880                  0880;                    CCCCC88                 00888888888888888
  GGGGGGGGGGGGGCCCC80                      080              .0000080i                    C88C880             i00000888888888888888
  GGGGGGGGGGGCCCCCC80      000000000       880       0000000000008801      000000000000008888888       00000000@008888888888888888
  CCCCGCCCGCCCCCCCC88      000000000      .880       00@00@000088888t      0000000000@0008888880       00000@000088888888888888888
  CCCCCCCCCCCCCCCCC88      0000000000000000880       000000088888888t      000000000000088888880       000000088888888888888888888
  CCCCCCCCCCCCCCCCC80      0888888000000000880       008888888888888t      008888888888888888880       088888888888888888888888888
  CCCCCCCCCCCCCCCCC80::;;,:8888888888888888888;,,,,.,888888CCCCCC888f.,:;,:888888888888888888880i,::::t888888888888888888888888888
  CCCCCCCCCCCCCCCC8800088888888CCCC88888888888000888888CCCCCCCCC88880000088888CCCCCCCCCCCCCC88800000088888888888888888888888888888
  CCCCCCCCCCCCCCCC88000888888CCCCCCCCCCCC88888000888888CCCCCCCCCC888000888888CCCCCCCCCCCCCC888800000088888888888888888888888888888
  CCCCCCCCCCCCCCCC8880008888CCCCCCCCCCCCCCC888880888888CCCCCCCCCC88880008888CCCCCCCCCCCCCCCC88880000088888888888888888888888888888
  CCCCCCCCCCCCCCCCC88888888CCCCCCCCCCCCCCCCCC888888888CCCCCCCCCCC8888888888CCCCCCCCCCCCCCCC888888888888888888888888888888888888888
  CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC8888888CCCCCCCCCCCCCCCC8888888888888888888888888888888888888888
  CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC88888888888888888888888888888888888888888
  CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC88888888888888888888888888888888888888888888
  CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC888888888888888888888888888888888888888888888
  CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC8CCCCC888888888888888888888888888888888888888888888
  CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC88888888888888888888888888888888888888888888888
  CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC88888C88C8888888888888888888888888888888888888888888888888888
  CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC8888888888888888888888888888888888888888888888888888888888888888888888888888800
  88CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC8888888888888888888888888888888888888888888888888888888888888888888888888888888880000
  88888CCCCCCCCCCCCCCCCCCCCCCCCC88888888C88888888888888888888888888888888888888888888888888888888888888888888888888888888888800000
  888888888888888C8888888CC8888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888880000000        
  `
  );
 