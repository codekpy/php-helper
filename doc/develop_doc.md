# PHP助手开发文档 #
开发人员务必仔细阅读
## 开发工具 ##
### 定义积木 ###
#### 使用Blockly Developer Tools ####
#### 链接：[http://blockly.daobanmojie.com/demos/blockfactory/](http://blockly.daobanmojie.com/demos/blockfactory/) ####
### 代码编辑器 ###
建议使用Web IDE或Visual Studio Code（需安装Git）。
## 文件介绍 ##
### kzhzbcphp.php ###
用来更新ide.html。当修改了积木，工作箱，代码转换器时，就需要运行kzhzbcphp.php来更新ide.html
### index.php ###
官网
### index.html ###
使用文档
### blockly ###
包含了原生blockly的代码
### blockly/media ###
存放blockly工作区媒体文件
### blockly/ini ###
定制blockly的地方。里面有三个文件夹，分别是workspace，block和site
### blockly/ini/workspace ###
存放工作区定义（workspace.xml workspace.js），工作箱定义（toolbox.xml），块库（library.xml）
### blockly/ini/block ###
存放积木定义（get.js）和积木转换器（get.js.js）
### blockly/ini/site ###
存放网站的一些图片
### blockly/colored_egg ###
存放彩蛋
## 注意事项 ##
1.	提交时**一定要提交到Dev分支！一定要提交到Dev分支！ 一定要提交到Dev分支！**
2.	如果提示存在**冲突**，请在讨论群中说明情况，说清文件名。**千万不要擅自建立分支！**
## 文档编写人员 ##
### CodeKpy ###
### ZouBochen ###