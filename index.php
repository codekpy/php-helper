<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <title>PHP助手——让世界没有难写的网页后端</title>
    <link rel="shortcut icon" href="./blockly/ini/site/Loge.svg"type="image/x-icon">
    <meta content="width=device-width,minimum-scale=1.0 initial-scale=1.0, maximum-scale=1.0,user-scalable=no"name="viewport">
</head>

<body>
<script src="https://lf1-cdn-tos.bytegoofy.com/obj/iconpark/icons_13540_39.cc6e8d42eca9877b333fd3835caa47fe.js"></script>
<style>
    body {
        background: rgb(246, 246, 246);
        margin: 0px;
        padding: 0px;
    }

    #main {
        width: 100vw;
        height: 800px;
        overflow: hidden;
        display: flex;
        justify-content: center;
        position: relative;
    }

    #banner  {
        width: 150vw;
        height: 800px;
        overflow: hidden;
        position: absolute;
        min-width: 1000px;
        background-color: #6d50f0;
        background: radial-gradient(rgba(109, 80, 240, 0.95), rgba(245, 248, 250, 0.15)), url('./blockly/ini/site/web-ui.svg') no-repeat 0% 20%/ cover;
        background-position: center 0;
        border-radius: 0 0 50% 50%;
    }

    #content {
        display: flex;
        align-items: center;
        height: 100%;
        width: 100vw;
        justify-content: center;
        position: absolute;
        flex-direction: column;
        color: #fff;
        filter: drop-shadow(0 0 30px #9C89FF);
        animation: a 1.3s both;
        -webkit-animation: a 1.3s both;
    }

    @keyframes a {
        0% {
            opacity: 0;
            transform: translateY(50px);
        }

        100% {
            opacity: 1;
            transform: translateY(0px);
        }
    }

    @-webkit-keyframes a {
        0% {
            opacity: 0;
            transform: translateY(50px);
        }

        100% {
            opacity: 1;
            transform: translateY(0px);
        }
    }

    .ver {
        border-width: 0;
        background: #9C89FFaa;
        color: white;
        width: 150px;
        font-size: 22px;
        height: 70px;
        border-radius: 10px;
        margin: 0 20px;
        display: flex;
        align-items: center;
        justify-content: space-evenly;
        align-content: center;
        transition: transform 0.3s, background-color 0.3s;
    }

    .ver:hover {
        transform: scale(1.1);
        background: #9C89FF11;
        cursor: pointer;
    }

    #thank,
    #form {
        width: 100vw;
        margin: 100px 0;
        display: flex;
        align-items: center;
        justify-content: center;
        flex-direction: column;
        align-content: center;
        color: #000;
        font-size: 25px;
        flex-wrap: wrap;
        animation: a 1.3s both;
        -webkit-animation: a 1.3s both;
    }

    *::-webkit-scrollbar {
        display: none;
    }

    * {
        scrollbar-width: none;
        -ms-overflow-style: none;
    }

    .card,
    .thankyou {
        margin: 10px;
        width: 250px;
        height: 200px;
        background: white;
        border-radius: 0.4em;
        display: inline-flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        transition: transform 0.3s;
    }

    .thankyou {
        width: 250px;
        height: 250px;
        justify-content: space-evenly;
    }

    .card:hover,
    .thankyou:hover {
        transform: scale(1.1);
        cursor: pointer;
    }

    #footer {
        width: calc(100vw - 40px);
        height: max-content;
        border: #fff solid 20px;
        background: white;
        display: flex;
        align-items: center;
        justify-content: center;
        align-content: center;
        flex-wrap: wrap;
        text-decoration: auto;
        color: currentColor;
    }

    .rouqi,
    .rongqi {
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        align-items: center;
        align-content: center;
    }

    .thank_pic {
        width: 100px;
        height: 100px;
    }

    @media only screen and (max-width: 1350px) {
        .rouqi {
            width: 810px;
        }

        .rongqi {
            width: 540px;
        }
    }

    @media only screen and (max-width: 810px) {

        .rouqi,
        .rongqi {
            width: 540px;
        }
    }

    @media only screen and (max-width: 540px) {

        .card,
        .thankyou {
            width: 200px;
            height: 200px;
        }

        .rouqi,
        .rongqi {
            width: calc(540px * 0.815);
        }
    }

    @media only screen and (max-width: 430px) {

        .card,
        .thankyou {
            width: 150px;
            height: 150px;
            font-size: 20px;
        }

        .rouqi,
        .rongqi {
            width: calc(540px * 0.63);
        }

        .thank_pic {
            width: 50px;
            height: 50px;
        }
    }
</style>
<div id="main">
    <div id="banner"></div>
    <div id="content">
        <img id="about-img" width="300"
             onclick="if(!Math.floor((Math.random()*10))){if(Math.floor(Math.random()*2)==1){var audio= new Audio ('https://creation.codemao.cn/445/kitten/d2ViXzIwMDJfNDM4NDAzXzY2NTQzNjVfMTY0NDMxNjEzNzUyNV9kYWFiNjE5Nw==');audio.play ();}else{var audio= new Audio ('https://creation.codemao.cn/445/kitten/d2ViXzIwMDJfNDM4NDAzXzY2NTQzNjVfMTY1Mzc5Mzg2NjE0OV9hYzBhMzY2Mw==');audio.play ();}}"
             src="./blockly/ini/site/ico.jpg" alt="PHP助手"></img>
        <p style="font-size: 30px;margin-block-start: 2em; margin-block-end: 3em;font-weight: bold;">
            让世界没有难写的后端
        </p>
        <!--div style="margin-block-end: 1em;">
            <a href='https://gitee.com/coco-central/waddle/stargazers'>
                <img src='https://gitee.com/coco-central/waddle/badge/star.svg?theme=white' alt='star'></img>
            </a>
            <a href='https://gitee.com/coco-central/waddle/members'>
                <img src='https://gitee.com/coco-central/waddle/badge/fork.svg?theme=white' alt='fork'></img>
            </a>
        </div-->
        <div id="buttons" style="display: flex;">
            <button onclick="window.open('./ide.html');" class="ver">
                <iconpark-icon width="35px" height="35px" name="release" color="white"></iconpark-icon>
                PHP助手

            </button>
            <button onclick="window.open('.\\blockly\\ini\\site\\thanks.jpg');" class="ver">
                <iconpark-icon width="35px" height="35px" name="beta" color="white"></iconpark-icon>
                捐赠
            </button>
        </div>
    </div>
</div>
<div id="form">
    <div class="rongqi">
        <div class="card" onclick="window.open('https://gitee.com/codekpy/php-helper/');">
            <iconpark-icon width="80px" height="80px" name="gitee"></iconpark-icon>
            Gitee仓库
        </div>
        <div class="card"
             onclick="window.open('.\\blockly\\ini\\site\\thanks.jpg');">
            <iconpark-icon width="80px" height="80px" name="wechat"></iconpark-icon>
            捐赠
        </div>
        <div class="card" onclick="window.open('./index.html');">官方文档</div>
    </div>
</div>

<div id="thank">
    <p style="font-size: 30px;font-weight:bold;">致谢人员</p>
    <div class="rouqi">
        <div class="thankyou" onclick="window.open('https://gitee.com/codekpy');"><img class="thank_pic"
                                                                                         style="border: transparent solid 3px;border-radius: 100%;"
                                                                                         src=".\blockly\ini\site\kzh.jpg">Codekpy
        </div>
        <div class="thankyou" onclick="window.open('https://gitee.com/zou-bochen');"><img class="thank_pic"
            style="border: transparent solid 3px;border-radius: 100%;"
            src=".\blockly\ini\site\zbc.png">邹帛辰
</div>
        <div class="thankyou" onclick="window.open('https://gitee.com/wangs_official');"><img class="thank_pic"
            style="border: transparent solid 3px;border-radius: 100%;"
            src=".\blockly\ini\site\zbc.png">诗岸单推人
</div>

    </div>
</div>

<div id="footer">
    <p>&nbsp;&nbsp;©️PHP助手 2022-<?php echo date("Y")?>&nbsp;&nbsp;</p>
    <p><a href="./index.html" target="_blank"
          style="text-decoration: auto; color: currentColor;"> 使用声明 </a></p>
    <p><a href="https://gitee.com/codekpy/php-helper/contributors?ref=master" target="_blank"
          style="text-decoration: auto; color: currentColor;">&nbsp;&nbsp;查看仓库全部贡献人员&nbsp;&nbsp;</a></p>
</div>
</body>

</html>