# PHP助手<br>
[![star](https://gitee.com/codekpy/php-helper/badge/star.svg?theme=gvp)](https://gitee.com/codekpy/php-helper/stargazers)
[![fork](https://gitee.com/codekpy/php-helper/badge/fork.svg?theme=gvp)](https://gitee.com/codekpy/php-helper/members)

我们基于谷歌的开源程序Blockly开发了此程序——PHP助手


## 使用PHP助手

PHP助手分为[在线版](http://www.codekpy.site/php-hepler-master/index.php)和[离线版](https://gitee.com/codekpy/php-helper/)以及[GithubPages版](https://wangs-offical.github.io/php-helper/ide.html)


### 安装PHP助手

在gitee的仓库页上克隆或下载本程序就好啦

```bash
本程序不用安装，直接下载就好啦
```


### 获取帮助
如果您遇到困难，可以加入我们的官方QQ群<br>

群号:607440618
### 使用教程

首先，将下载的.zip压缩包解压。  <br>

进入目录，找到index.html和index.php文件。<br>

如果您的电脑有本地服务器，建议您先运行一下index.php。</br>

如果没有，也没有关系，直接在浏览器打开index.html即可。<br>


### 分支

目前，PHP助手只有master分支，待项目更成熟一些，便会新建develop <br>

### 汇报bug

请加入QQ群或者在此项目的评论区反馈哦<br>

## 开发指南

目录说明:
blockly文件夹是Google's blockly源码的文件夹 <br>
    ini文件夹是PHP助手的积木定义（get.js），积木转换代码定义（get.js.js）工作区（workspace.xml workspace.js），工作箱（toolbox.xml）的定义<br>
media文件夹是Google's blockly的媒体文件<br>
index.php 把积木定义，工作区，工作箱的定义代码写入index.html文件夹，这样就可以方便的运行PHP助手了<br>
## 一键部署功能
先将helper.php部署在服务器上，然后会一步一步指引您的
[下载helper.php](.http://www.codekpy.site/php-helper-master/d.html)

## 免责声明


本开源程序根目录下网页`index.php`的源代码由[CoCo中控台](https://gitee.com/coco-central)制作，
[PHP助手贡献人员](https://gitee.com/codekpy/php-helper/contributors?ref=master)修改，
运营于[PHP助手官网](http://www.codekpy.site/php-helper-master/index.php)，
借鉴[微信](https://weixin.qq.com/)的设计风格。

在不违反法律的前提下，该源代码 **（限于学习、公益用途）** ，
在使用中，**不强制要求注明制作方**。
产生的法律问题，PHP助手贡献人员**不负连带责任**。

![图片](blockly/ini/site/ico.jpg)
特此声明